# post_gen_project.py
import os
import shutil

from pathlib import Path

# Current path
path = Path(os.getcwd())

# Source path
parent_path = path.parent.absolute()

def remove(filepath):
    if os.path.isfile(filepath):
        os.remove(filepath)
    elif os.path.isdir(filepath):
        shutil.rmtree(filepath)

create_instance_choice = '{{ cookiecutter.create_instance }}'

generate_instance = (create_instance_choice == 'y' or create_instance_choice == 'yes' or create_instance_choice == 'Y')

if not generate_instance:
    folder_path = os.path.join(
            parent_path, 
            '{{ cookiecutter.detector_name }}Project', 
            '{{ cookiecutter.detector_name }}Instance'
    )
    remove(folder_path)